namespace StoryboardAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMentorData : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT Mentors ON");

            Sql("INSERT INTO Mentors (Id, FirstName, SecondName, DateOfBirth, Username, Password, Email, MentorUniqueNumber) VALUES " +
                "(1, 'Natalia', 'Salmanova', '1995-02-22 00:00:00', 'Nat', 'Natalia123!', 'nsalmanova15@gmail.com', 456 )");

            Sql("INSERT INTO Mentors (Id, FirstName, SecondName, DateOfBirth, Username, Password, Email, MentorUniqueNumber) VALUES " +
                "(2, 'Sam', 'Peterson', '1982-06-02 00:00:00', 'Sam', 'Sam123!', 'sammy@gmail.com', 632 )");
        }
        
        public override void Down()
        {
        }
    }
}
