namespace StoryboardAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMenteeData : DbMigration
    {
        public override void Up()
        {
            Sql("SET IDENTITY_INSERT Mentees ON");

            Sql("INSERT INTO Mentees (Id, FirstName, SecondName, DateOfBirth, Username, Password, Email, mentor_Id) VALUES " +
                "(1, 'Aisling', 'Carolan', '1996-01-13 00:00:00', 'Ash', 'Ash12345!', 'ais@gmail.com', 1)");

            Sql("INSERT INTO Mentees (Id, FirstName, SecondName, DateOfBirth, Username, Password, Email, mentor_Id) VALUES " +
               "(2, 'Michael', 'Stafford', '2002-06-13 00:00:00', 'Mike', 'Mike456!', 'michael@gmail.com', 2)");

        }

        public override void Down()
        {
        }
    }
}
