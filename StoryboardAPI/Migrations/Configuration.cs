namespace StoryboardAPI.Migrations
{
    using Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<StoryboardAPI.DataModel.StoryboardContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(StoryboardAPI.DataModel.StoryboardContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            //context.Mentors.AddOrUpdate(
            //    m => m.Id,
            //    new Mentor { FirstName = "Natalia", SecondName = "Salmanova", DateOfBirth = DateTime.Now.Date, Username = "Nat", Password="Natalia123!", Email="nsalmanova15@gmail.com", MentorUniqueNumber = 145  }
            //    );
        }
    }
}
