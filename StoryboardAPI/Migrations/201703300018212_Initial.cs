namespace StoryboardAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Feedbacks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(),
                        nb_Id = c.Int(nullable: false),
                        sb_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Noticeboards", t => t.nb_Id, cascadeDelete: true)
                .ForeignKey("dbo.Storyboards", t => t.sb_Id, cascadeDelete: true)
                .Index(t => t.nb_Id)
                .Index(t => t.sb_Id);
            
            CreateTable(
                "dbo.Noticeboards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        NoticeboardName = c.String(),
                        StickyNotes = c.String(),
                        Image = c.Binary(),
                        FeedbackId = c.Int(nullable: false),
                        mentee_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Mentees", t => t.mentee_Id, cascadeDelete: true)
                .Index(t => t.mentee_Id);
            
            CreateTable(
                "dbo.Mentees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        SecondName = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        Username = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                        mentor_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Mentors", t => t.mentor_Id, cascadeDelete: true)
                .Index(t => t.mentor_Id);
            
            CreateTable(
                "dbo.Mentors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        SecondName = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        Username = c.String(),
                        Password = c.String(),
                        Email = c.String(),
                        MentorUniqueNumber = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Storyboards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StoryboardName = c.String(),
                        StickyNotes = c.String(),
                        Image = c.Binary(),
                        FeedbackId = c.Int(nullable: false),
                        mentor_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Mentors", t => t.mentor_Id, cascadeDelete: false)
                .Index(t => t.mentor_Id);
            
            CreateTable(
                "dbo.Journals",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        JournalTitle = c.String(),
                        Text = c.String(),
                        Image = c.Binary(),
                        FeedbackId = c.Int(nullable: false),
                        mentor_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Mentors", t => t.mentor_Id, cascadeDelete: true)
                .Index(t => t.mentor_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Journals", "mentor_Id", "dbo.Mentors");
            DropForeignKey("dbo.Feedbacks", "sb_Id", "dbo.Storyboards");
            DropForeignKey("dbo.Feedbacks", "nb_Id", "dbo.Noticeboards");
            DropForeignKey("dbo.Noticeboards", "mentee_Id", "dbo.Mentees");
            DropForeignKey("dbo.Mentees", "mentor_Id", "dbo.Mentors");
            DropForeignKey("dbo.Storyboards", "mentor_Id", "dbo.Mentors");
            DropIndex("dbo.Journals", new[] { "mentor_Id" });
            DropIndex("dbo.Storyboards", new[] { "mentor_Id" });
            DropIndex("dbo.Mentees", new[] { "mentor_Id" });
            DropIndex("dbo.Noticeboards", new[] { "mentee_Id" });
            DropIndex("dbo.Feedbacks", new[] { "sb_Id" });
            DropIndex("dbo.Feedbacks", new[] { "nb_Id" });
            DropTable("dbo.Journals");
            DropTable("dbo.Storyboards");
            DropTable("dbo.Mentors");
            DropTable("dbo.Mentees");
            DropTable("dbo.Noticeboards");
            DropTable("dbo.Feedbacks");
        }
    }
}
