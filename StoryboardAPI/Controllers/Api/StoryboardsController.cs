﻿using StoryboardAPI.DataModel;
using StoryboardAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace StoryboardAPI.Controllers.Api
{
    public class StoryboardsController : ApiController
    {
        private StoryboardContext sbContext;

        public StoryboardsController()
        {
            sbContext = new StoryboardContext();
        }

        // GET Api/Storyboards
        public IEnumerable<Storyboard> GetStoryboards()
        {
            return sbContext.Storyboards.ToList();
        }

        // Get /Api/Storyboard/1
        [HttpGet]
        public Storyboard GetStoryboard(int id, int mentorId)
        {
            var storyboard = sbContext.Storyboards.SingleOrDefault(s => s.Id == id);

            //return a page with error message that is user friendly
            if (storyboard == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            return storyboard;
        }

        //get all storyboard ids of the logged in user
        [HttpGet]
        public List<int> GetStoryboardsOfLoggedUser(int mentor_id)
        {
            IEnumerable<Storyboard> storyboard = sbContext.Storyboards.Where(s => s.mentor.Id == mentor_id);

            //List<Storyboard> storyboard = sbContext.Storyboards.ToList();
            List<Storyboard> st = storyboard.ToList();

            List<int> storyboardIds = new List<int>();

            if (st != null)
            {
                for (int i = 0; i < st.Count; i++)
                {
                    storyboardIds.Add(st[i].Id);
                }
            }
            else
            {
                storyboardIds.Add(0);
            }
            return storyboardIds;
        }

        //get sticky note content
        [HttpGet]
        public string GetStickyNote(int id)
        {
            var storyboard = sbContext.Storyboards.SingleOrDefault(s => s.Id == id);

            //return a page with error message that is user friendly
            if (storyboard == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            return storyboard.StickyNotes;
        }

        //Put add sticky note (update existsing storyboard)
        [HttpPut]
        public void UpdateStoryboardStickyNote(int id, Storyboard st)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var storyboardInDb = sbContext.Storyboards.SingleOrDefault(s => s.Id == id);

            if (storyboardInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            storyboardInDb.StickyNotes = st.StickyNotes;
            storyboardInDb.mentor.Id = 1;
            sbContext.SaveChanges();
        }

        // Post /Api/Storyboard
        [HttpPost]
        public Storyboard AddStoryboard(Storyboard storyboard)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            sbContext.Storyboards.Add(storyboard);
            sbContext.SaveChanges();

            return storyboard;
        }

        // Delete /Api/Storyboards/1
        [HttpDelete]
        public void DeleteStoryboard(int id)
        {
            var storyboardInDb = sbContext.Storyboards.SingleOrDefault(s => s.Id == id);

            if (storyboardInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            sbContext.Storyboards.Remove(storyboardInDb);
            sbContext.SaveChanges();
        }
    }
}
