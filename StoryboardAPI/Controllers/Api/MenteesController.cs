﻿using StoryboardAPI.DataModel;
using StoryboardAPI.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace StoryboardAPI.Controllers.Api
{
    public class MenteesController : ApiController
    {
        private StoryboardContext sbContext;

        public MenteesController()
        {
            sbContext = new StoryboardContext();
        }

        // GET Api/Mentors
        public IEnumerable<Mentee> GetMentees()
        {
            return sbContext.Mentees.ToList();
        }

        // Get /Api/Mentor/1
        public Mentee GetMentee(int id)
        {
            var mentee = sbContext.Mentees.SingleOrDefault(m => m.Id == id);

            //return a page with error message that is user friendly
            if (mentee == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            return mentee;
        }

        // Get /Api/Mentees/username && password
        [HttpGet]
        public int LoginMentee(string email, string password)
        {
            var mentee = sbContext.Mentees.SingleOrDefault(m => m.Email == email && m.Password == password);

            //return a page with error message that is user friendly
            if (mentee == null)
                return 0;

            return mentee.Id;
        }

        //Get /Api/Mentees/mentorid/1
        [HttpGet]
        public string GetMenteeByMentorId(int mentorId)
        {
            var mentee = sbContext.Mentees.SingleOrDefault(m => m.mentor.Id == mentorId);

            if (mentee == null)
                return null;

            return mentee.FirstName + " " + mentee.SecondName;
        }

        // Post /Api/Mentor
        [HttpPost]
        public Mentee AddMentee(Mentee mentee)
        {
            if (!ModelState.IsValid)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            sbContext.Mentees.Add(mentee);
            sbContext.SaveChanges();

            return mentee;
        }

        // Delete /Api/Mentors/1
        [HttpDelete]
        public void DeleteMentee(int id)
        {
            var menteeInDb = sbContext.Mentees.SingleOrDefault(m => m.Id == id);

            if (menteeInDb == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            sbContext.Mentees.Remove(menteeInDb);
            sbContext.SaveChanges();
        }
    }
}
