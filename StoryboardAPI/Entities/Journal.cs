﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StoryboardAPI.Entities
{
    public class Journal
    {
        public int Id { get; set; }
        public string JournalTitle { get; set; }
        public string Text { get; set; }
        public byte[] Image { get; set; }
        [Required]
        public Mentor mentor { get; set; }
        public int FeedbackId { get; set; }
    }
}