﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StoryboardAPI.Entities
{
    public class Feedback
    {
        public int Id { get; set; }
        public string Text { get; set; }
        [Required]
        public Storyboard sb { get; set; }
        [Required]
        public Noticeboard nb { get; set; }
    }
}