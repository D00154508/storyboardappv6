﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StoryboardAPI.Entities
{
    public class Noticeboard
    {
        public int Id { get; set; }
        public string NoticeboardName { get; set; }
        public string StickyNotes { get; set; }
        public byte[] Image { get; set; }
        [Required]
        public Mentee mentee { get; set; }
        public int FeedbackId { get; set; }
    }
}