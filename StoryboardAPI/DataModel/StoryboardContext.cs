﻿using StoryboardAPI.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace StoryboardAPI.DataModel
{
    public class StoryboardContext : DbContext
    {
        public DbSet<Mentor> Mentors { get; set; }
        public DbSet<Mentee> Mentees { get; set; }
        public DbSet<Storyboard> Storyboards { get; set; }
        public DbSet<Noticeboard> Noticeboards { get; set; }
        public DbSet<Journal> Journals { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
    }
}