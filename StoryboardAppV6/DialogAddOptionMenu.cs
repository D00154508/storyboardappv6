using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using StoryboardAppV6.AddToStoryboard;

namespace StoryboardAppV6
{
    class DialogAddOptionMenu : DialogFragment
    {
        private Button mBtnAddPic;
        private Button mBtnAddNote;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.dialog_menu_option, container, false);

            mBtnAddPic = view.FindViewById<Button>(Resource.Id.btnDialogAddPic);
            mBtnAddNote = view.FindViewById<Button>(Resource.Id.btnDialogAddNote);

            Intent intent = new Intent(Application.Context, typeof(ChooseImageActivity));
            intent.SetFlags(ActivityFlags.NewTask);

            Intent intentNote = new Intent(Application.Context, typeof(AddNoteActivity));
            intentNote.SetFlags(ActivityFlags.NewTask);

            mBtnAddPic.Click += delegate
            {
                Application.Context.StartActivity(intent);
            };

            mBtnAddNote.Click += delegate
            {
                Application.Context.StartActivity(intentNote);
            };

            return view;
        }
    }
}