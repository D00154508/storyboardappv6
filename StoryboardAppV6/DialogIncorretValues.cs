using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace StoryboardAppV6
{
    class DialogIncorretValues : DialogFragment
    {
        private Button btnOk;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.dialog_incorrect_values, container, false);

            btnOk = view.FindViewById<Button>(Resource.Id.btnDialogOk);

            Intent intent = new Intent(Android.App.Application.Context, typeof(MainActivity));
            intent.SetFlags(ActivityFlags.NewTask);

            btnOk.Click += delegate
            {
                Application.Context.StartActivity(intent);
            };

            return view;
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            Dialog.Window.RequestFeature(WindowFeatures.NoTitle); //Sets the title bar to invisible
            base.OnActivityCreated(savedInstanceState);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.dialog_animation; //set the animation
        }
    }
}