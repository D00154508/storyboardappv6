using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using StoryboardAppV4;

namespace StoryboardAppV6
{
    class DialogStoryboardMenu : DialogFragment
    {
        private Button mBtnPinBoard;
        private Button mBtnFlipBoard;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.dialog_create_storyboard, container, false);

            mBtnPinBoard = view.FindViewById<Button>(Resource.Id.btnDialogPinboard);
            mBtnFlipBoard = view.FindViewById<Button>(Resource.Id.btnDialogFlipboard);

            Intent intent = new Intent(Android.App.Application.Context, typeof(SwipeActivity));
            intent.SetFlags(ActivityFlags.NewTask);

            Intent intentPin = new Intent(Android.App.Application.Context, typeof(CreatePinboardActivity));
            intentPin.SetFlags(ActivityFlags.NewTask);

            mBtnPinBoard.Click += delegate
            {
                Application.Context.StartActivity(intentPin);
            };

            mBtnFlipBoard.Click += delegate 
            {
                Application.Context.StartActivity(intent);
            };

            return view;
        }

    }
}