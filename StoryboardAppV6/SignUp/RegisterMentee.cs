using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using StoryboardAPI.Entities;
using System.Net.Http;
using Newtonsoft.Json;

namespace StoryboardAppV6.SignUp
{
    class RegisterMentee : DialogFragment
    {
        private EditText mTxtFirstName;
        private EditText mTxtSecondName;
        private EditText mDateofBirth;
        private EditText mTxtUsername;
        private EditText mTxtPassword;
        private EditText mTxtEmail;
        //mentee only
        private EditText mMentorUMN;

        private Button mBtnSignUp;

        public async Task AddMentee(Mentee mentee)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://storyboardapi20170314091148.azurewebsites.net/Api/Mentees/AddMentee");

                string json = JsonConvert.SerializeObject(mentee);
                var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PostAsync(client.BaseAddress, stringContent);

                bool isSuccess = response.IsSuccessStatusCode;

                Intent intent = new Intent(Application.Context, typeof(MenteeActivity));
                intent.SetFlags(ActivityFlags.NewTask);

                if (isSuccess)
                {
                    Application.Context.StartActivity(intent);
                }
            }
        }

        public async Task<string> GetMentorId(int umn)
        {
            using (var client = new HttpClient())
            {
                var url = new Uri("http://storyboardapi20170314091148.azurewebsites.net/Api/Mentors?umn=" + umn);
                var result = await client.GetStringAsync(url);

                return result;
            }
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.dialog_register_mentee, container, false);

            mTxtFirstName = view.FindViewById<EditText>(Resource.Id.txtFirstName);
            mTxtSecondName = view.FindViewById<EditText>(Resource.Id.txtSurname);
            mTxtEmail = view.FindViewById<EditText>(Resource.Id.txtEmail);
            mDateofBirth = view.FindViewById<EditText>(Resource.Id.txtDoB);
            mTxtUsername = view.FindViewById<EditText>(Resource.Id.txtUsername);
            mTxtPassword = view.FindViewById<EditText>(Resource.Id.txtPassword);
            mMentorUMN = view.FindViewById<EditText>(Resource.Id.txtMentorUMN);
            mBtnSignUp = view.FindViewById<Button>(Resource.Id.btnDialogEmail);

            //GetButton.Click += GetDept(id); 
            mBtnSignUp.Click += async (sender, args) =>
            {
                DateTime date = Convert.ToDateTime("1995-02-22 00:00:00");
                int umn = Convert.ToInt32(mMentorUMN.Text);

                string mentorId = await GetMentorId(umn);
                int mentorIdInt = Convert.ToInt32(mentorId);

                Mentee ment = new Mentee();
                ment.FirstName = mTxtFirstName.Text;
                ment.SecondName = mTxtSecondName.Text;
                ment.Email = mTxtEmail.Text;
                ment.DateOfBirth = date;
                ment.Username = mTxtUsername.Text;
                ment.Password = mTxtPassword.Text;
                ment.mentor.Id = mentorIdInt;

                await AddMentee(ment);
                
            };

            return view;
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            Dialog.Window.RequestFeature(WindowFeatures.NoTitle); //Sets the title bar to invisible
            base.OnActivityCreated(savedInstanceState);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.dialog_animation; //set the animation
        }
    }
}