using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using StoryboardAPI.Entities;
using System.Net.Http;
using Newtonsoft.Json;
using StoryboardAppV4;

namespace StoryboardAppV6.SignUp
{
    class RegisterMentor : DialogFragment
    {
        private EditText mTxtFirstName;
        private EditText mTxtSecondName;
        private EditText mDateofBirth;
        private EditText mTxtUsername;
        private EditText mTxtPassword;
        private EditText mTxtEmail;
        //mentor only
        private EditText mMentorUniqueNumber;

        private Button mBtnSignUp;

        public async Task AddMentor(Mentor mentor)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://storyboardapi20170314091148.azurewebsites.net/Api/Mentors/AddMentor");

                string json = JsonConvert.SerializeObject(mentor);
                var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PostAsync(client.BaseAddress, stringContent);

                bool isSuccess = response.IsSuccessStatusCode;

                Intent intent = new Intent(Application.Context, typeof(MentorActivity));
                intent.SetFlags(ActivityFlags.NewTask);

                if (isSuccess)
                {
                    Application.Context.StartActivity(intent);
                }
            }
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.dialog_register_mentor, container, false);

            mTxtFirstName = view.FindViewById<EditText>(Resource.Id.txtFirstName);
            mTxtSecondName = view.FindViewById<EditText>(Resource.Id.txtSurname);
            mTxtEmail = view.FindViewById<EditText>(Resource.Id.txtEmail);
            mDateofBirth = view.FindViewById<EditText>(Resource.Id.txtDoB);
            mTxtUsername = view.FindViewById<EditText>(Resource.Id.txtUsername);
            mTxtPassword = view.FindViewById<EditText>(Resource.Id.txtPassword);
            mMentorUniqueNumber = view.FindViewById<EditText>(Resource.Id.txtUniqueNum);
            mBtnSignUp = view.FindViewById<Button>(Resource.Id.btnDialogEmail);

            //GetButton.Click += GetDept(id); 
            mBtnSignUp.Click += async (sender, args) =>
            {       
                int mun = Convert.ToInt32(mMentorUniqueNumber.Text);
                DateTime date = Convert.ToDateTime("1995-02-22 00:00:00");

                //if(mMentorUniqueNumber)
                Mentor men = new Mentor();
                men.FirstName = mTxtFirstName.Text;
                men.SecondName = mTxtSecondName.Text;
                men.Email = mTxtEmail.Text;
                men.DateOfBirth = date;
                men.Username = mTxtUsername.Text;
                men.Password = mTxtPassword.Text;
                men.MentorUniqueNumber = mun;

                await AddMentor(men);               
            };

            return view;
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            Dialog.Window.RequestFeature(WindowFeatures.NoTitle); //Sets the title bar to invisible
            base.OnActivityCreated(savedInstanceState);
            Dialog.Window.Attributes.WindowAnimations = Resource.Style.dialog_animation; //set the animation
        }
    }
}