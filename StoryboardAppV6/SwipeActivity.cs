using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using StoryboardAppV6;
using Android.Util;

namespace StoryboardAppV4
{
    [Activity(Label = "SwipeActivity")]
    public class SwipeActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.Main);
            //Swipe 
            FragmentTransaction transaction = FragmentManager.BeginTransaction();
            SlidingTabsFragment fragment = new SlidingTabsFragment();
            transaction.Replace(Resource.Id.sample_content_fragment, fragment);
            transaction.Commit();
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.actionbar_main, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        //public void doPinboardClick()
        //{
        //    // Do stuff here.
        //    Log.i("FragmentAlertDialog", "Pinbard click!");
        //}

        //public void doFlipboardClick()
        //{
        //    // Do stuff here.
        //    Log.i("FragmentAlertDialog", "Flipboard click!");
        //}
    }
}