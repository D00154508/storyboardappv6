
using Android.App;
using Android.OS;

namespace StoryboardAppV6
{
    [Activity(Label = "MenteeActivity", Theme = "@android:style/Theme.Holo.Light.NoActionBar")]
    public class MenteeActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.mentee_page);
            // Create your application here

            var menu = FindViewById<FlyOutContainer>(Resource.Id.FlyOutContainer);
            var menuButton = FindViewById(Resource.Id.MenuButton);
            menuButton.Click += (sender, e) =>
            {
                menu.AnimatedOpened = !menu.AnimatedOpened;
            };
        }
    }
}