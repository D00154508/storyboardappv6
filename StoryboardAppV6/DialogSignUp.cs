using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using StoryboardAppV6;
using StoryboardAPI.Entities;
using static Android.Provider.SyncStateContract;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using StoryboardAppV6.SignUp;

namespace StoryboardAppV4
{
    class DialogSignUp : DialogFragment
    {
        private Button mBtnRegMentor;
        private Button mBtnRegMentee;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.dialog_sign_up, container, false);

            mBtnRegMentor = view.FindViewById<Button>(Resource.Id.btnDialogRegMentor);
            mBtnRegMentee = view.FindViewById<Button>(Resource.Id.btnDialogRegMentee);

            mBtnRegMentor.Click += delegate
            {
                FragmentTransaction transaction = FragmentManager.BeginTransaction();
                RegisterMentor regMentor = new RegisterMentor();
                regMentor.Show(transaction, "dialog fragment");
            };

            mBtnRegMentee.Click += delegate
            {
                FragmentTransaction transaction = FragmentManager.BeginTransaction();
                RegisterMentee regMentee = new RegisterMentee();
                regMentee.Show(transaction, "dialog fragment");
            };

            return view;
        }       
    }
}