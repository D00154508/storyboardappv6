using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using StoryboardAppV4;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Com.Nostra13.Universalimageloader.Core;
using Android.Support.V7.App;

using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using System;
using System.Threading.Tasks;
using System.Net.Http;

namespace StoryboardAppV6
{
    [Activity(Label = "ViewMenteesActivity")]
    public class ViewMenteesActivity : Activity
    {
        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.view_mentee_page);

            string mentorId = Intent.GetStringExtra("MentorId") ?? "Data not available";
            //int mentorId = Convert.ToInt32(text);
            await DisplayMentees(mentorId);

        }

        private async Task DisplayMentees(string mentorid)
        {
            using (var client = new HttpClient())
            {
                var url = "http://storyboardapi20170314091148.azurewebsites.net/api/Mentees?mentorId=" + mentorid;
                var result = await client.GetStringAsync(url);
                FindViewById<TextView>(Resource.Id.textViewMentee1).Text = result;
            }
        }
    }
}