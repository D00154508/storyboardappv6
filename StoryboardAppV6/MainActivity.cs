﻿using Android.App;
using Android.Widget;
using Android.OS;
using StoryboardAppV4;
using System;
using System.Threading.Tasks;
using System.Net.Http;
using Android.Content;

namespace StoryboardAppV6
{
    [Activity(Label = "StoryboardAppV6", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private Button mBtnSignUp;
        private Button mBtnSignIn;
        private EditText email;
        private EditText password;
        private bool success = false;
        private bool success2 = false;
        public string result;
        public string result2;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.Main_Login);
            //Login
            mBtnSignUp = FindViewById<Button>(Resource.Id.btnSignUp);
            mBtnSignIn = FindViewById<Button>(Resource.Id.btnSignIn);

            mBtnSignIn.Click += async (sender, args) =>
            {
                email = FindViewById<EditText>(Resource.Id.txtEmailLogin);
                password = FindViewById<EditText>(Resource.Id.txtPasswordLogin);

                string em = email.Text;
                string pw = password.Text;

                await Authenticate(em, pw);
                
                //bring to home page
                if(success)
                {
                    var mentorActivity = new Intent(this, typeof(MentorActivity));
                    mentorActivity.PutExtra("MyData", result);
                    StartActivity(mentorActivity);
                }
                else if(success2)
                {
                    StartActivity(typeof(MenteeActivity));
                }
                else
                {
                    //a dialog to say login or password incorrect
                    //Pull up dialog
                    FragmentTransaction transaction = FragmentManager.BeginTransaction();
                    DialogIncorretValues incorrectValuesDialog = new DialogIncorretValues();
                    incorrectValuesDialog.Show(transaction, "dialog fragment");
                }

            };

            mBtnSignUp.Click += (object sender, EventArgs args) =>
            {
                //Pull up dialog
                FragmentTransaction transaction = FragmentManager.BeginTransaction();
                DialogSignUp signUpDialog = new DialogSignUp();
                signUpDialog.Show(transaction, "dialog fragment");
            };


        }

        private async Task Authenticate(string em, string pw)
        {           
            using (var client = new HttpClient())
            {
                var url = "http://storyboardapi20170314091148.azurewebsites.net/Api/Mentors?email=" + em + "&password=" + pw;
                result = await client.GetStringAsync(url);

                if(!result.Equals("0"))
                {
                    success = true;
                }
                else
                {
                    var url2 = "http://storyboardapi20170314091148.azurewebsites.net/Api/Mentees?email=" + em + "&password=" + pw;
                    result2 = await client.GetStringAsync(url2);

                    if(!result2.Equals("0"))
                    {
                        success2 = true;
                    }
                    else
                    {
                        success2 = false;
                    }
                }               
            }          
        }
    }
}

