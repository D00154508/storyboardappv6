using System;
using System.Text;

using Android.App;
using Android.OS;
using Android.Widget;
using System.Threading.Tasks;
using StoryboardAPI.Entities;
using System.Net.Http;
using Newtonsoft.Json;

namespace StoryboardAppV6
{
    [Activity(Label = "CreatePinboardActivity")]
    public class CreatePinboardActivity : Activity
    {
        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.StoryboardLayout);

            Storyboard st = new Storyboard();
            st.Id = 1;
            st.StoryboardName = null;
            st.StickyNotes = null;
            st.Image = null;
            st.FeedbackId = 3;

            //Mentor m = await GetMentor(1); // id

            st.mentor = await GetMentor(1);

            await AddStoryboard(st);
            //add a toast to say that storyboard has been created
            OnTap();
        }

        void OnTap()
        {
            RelativeLayout rlayout = (RelativeLayout)FindViewById(Resource.Id.mainLayout);
            rlayout.Click += delegate 
            {
                //Pull up dialog
                FragmentTransaction transaction = FragmentManager.BeginTransaction();
                DialogAddOptionMenu addOptionMenuDialog = new DialogAddOptionMenu();
                addOptionMenuDialog.Show(transaction, "dialog fragment");
            };
        }

        private async Task AddStoryboard(Storyboard storyboard)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://storyboardapi20170314091148.azurewebsites.net/Api/Storyboards/AddStoryboard");

                string json = JsonConvert.SerializeObject(storyboard);
                var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PostAsync(client.BaseAddress, stringContent);  
            }
        }

        private async Task<Mentor> GetMentor(int id)
        {
            Mentor m;
            using (var client = new HttpClient())
            {
                var url = "http://storyboardapi20170314091148.azurewebsites.net/Api/Mentors/" + id;
                var mentor = await client.GetStringAsync(url);

                m = JsonConvert.DeserializeObject<Mentor>(mentor);

            }
            return m;
        }
    }  
}