using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using StoryboardAppV6.SportMenu;

namespace StoryboardAppV6
{
    class DialogSportSubMenu : DialogFragment
    {
        private Button mBtnGAA;
        private Button mBtnRugby;
        private Button mBtnSoccer;
        private Button mBtnUFC;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);

            var view = inflater.Inflate(Resource.Layout.sub_sport_menu, container, false);

            mBtnGAA = view.FindViewById<Button>(Resource.Id.btnDialogGAA);
            mBtnRugby = view.FindViewById<Button>(Resource.Id.btnDialogRugby);
            mBtnSoccer = view.FindViewById<Button>(Resource.Id.btnDialogSoccer);
            mBtnUFC = view.FindViewById<Button>(Resource.Id.btnDialogUFC);

            Intent intent = new Intent(Application.Context, typeof(GAAActivity));
            intent.SetFlags(ActivityFlags.NewTask);

            mBtnGAA.Click += delegate
            {
                Application.Context.StartActivity(intent);
            };

            //mBtnRugby.Click += delegate
            //{

            //};

            return view;
        }
    }
}