using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using static Android.Views.View;

namespace StoryboardAppV6
{
    [Activity(Label = "StoryboardActivity")]
    public class StoryboardActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.StoryboardLayout);
            SetContentView(Resource.Layout.slide_menu_layout);

            OnTap();
        }

        void OnTap()
        {
            RelativeLayout rlayout = (RelativeLayout)FindViewById(Resource.Id.mainLayout);
            rlayout.Click += delegate (object sender, EventArgs e)
            {
                
            };
        }
    }
}