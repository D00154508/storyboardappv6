using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using StoryboardAPI.Entities;

namespace StoryboardAppV6.AddToStoryboard
{
    [Activity(Label = "AddNoteActivity")]
    public class AddNoteActivity : Activity
    {
        private TextView mTextView;
        private string mString;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.add_sticky_note);   

            Button btnOk = FindViewById<Button>(Resource.Id.btnAddNoteOk);

            btnOk.Click += async delegate
            {
                EditText note = FindViewById<EditText>(Resource.Id.addText);
                string noteTxt = note.Text;
                await AddStickyNote(noteTxt);
            };
            // Create your application here
        }

        private async Task AddStickyNote(string note)
        {
            using (var client = new HttpClient())
            {
                var url = "http://storyboardapi20170314091148.azurewebsites.net/Api/Storyboards/" + 1 + "?mentorId=" + 1;
                
                var result = await client.GetStringAsync(url);

                Storyboard s = JsonConvert.DeserializeObject<Storyboard>(result);

                s.StickyNotes = note;
                //s.mentor.Id = 1;

                //client.BaseAddress = new Uri("http://storyboardapi20170314091148.azurewebsites.net/Api/Storyboards/" + 1);

                string json = JsonConvert.SerializeObject(s);
                var stringContent = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PutAsync(string.Concat("http://storyboardapi20170314091148.azurewebsites.net/Api/Storyboards/" + 1), stringContent);
                //var result = await client.PutAsJsonAsync(url, );

                mTextView = (TextView)FindViewById(Resource.Id.textViewStickynote);
                mString = result;
                mTextView.Text = mString;

            }
        }
    }
}