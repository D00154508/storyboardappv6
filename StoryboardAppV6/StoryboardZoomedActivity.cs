using System;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using System.Threading.Tasks;
using System.Net.Http;

namespace StoryboardAppV6
{
    [Activity(Label = "StoryboardZoomedActivity")]
    public class StoryboardZoomedActivity : Activity
    {
        private TextView mTextView;
        private string mString;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.zoomed_storyboard);

            //id of the logged in user
            string text = Intent.GetStringExtra("StoryboardId") ?? "Data not available";
            int stId = Convert.ToInt32(text);

            await GetStickyNote(stId);

        }

        private async Task GetStickyNote(int stId)
        {
            using (var client = new HttpClient())
            {
                var url = "http://storyboardapi20170314091148.azurewebsites.net/Api/Storyboards/" + stId;
                var result = await client.GetStringAsync(url);

                mTextView = (TextView)FindViewById(Resource.Id.textViewStickynote);
                mString = result;
                mTextView.Text = mString;

            }
        }
    }
}