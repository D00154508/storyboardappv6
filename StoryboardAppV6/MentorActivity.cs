
using Android.App;
using Android.Content;
using Android.OS;
using StoryboardAppV6;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace StoryboardAppV4
{
    [Activity(Label = "MentorActivity", Theme = "@android:style/Theme.Holo.Light.NoActionBar")]
    public class MentorActivity : Activity
    {
        public int mentorLoggedInId;

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.mentor_page);
            // Create your application here 

            var menu = FindViewById<FlyOutContainer>(Resource.Id.FlyOutContainer);
            var menuButton = FindViewById(Resource.Id.MenuButton);
            menuButton.Click += (sender, e) =>
            {
                menu.AnimatedOpened = !menu.AnimatedOpened;
            };

            string text = Intent.GetStringExtra("MyData") ?? "Data not available";
            mentorLoggedInId = Convert.ToInt32(text);

            await GetStoryboardsOfUser();

            var optionBtnViewMentees = FindViewById(Resource.Id.linearLayout2);
            optionBtnViewMentees.Click += delegate
            {
                var viewMenteeActivity = new Intent(this, typeof(ViewMenteesActivity));
                viewMenteeActivity.PutExtra("MentorId", text);
                StartActivity(viewMenteeActivity);
            };

            var optionBtnSport = FindViewById(Resource.Id.linearLayout3);
            optionBtnSport.Click += delegate
            {
                //Pull up menu dialog
                FragmentTransaction transaction = FragmentManager.BeginTransaction();
                DialogSportSubMenu sportSubMenu = new DialogSportSubMenu();
                sportSubMenu.Show(transaction, "dialog fragment");
            };

            var optionBtnLogout = FindViewById(Resource.Id.linearLayout11);
            optionBtnLogout.Click += delegate
            {
                StartActivity(typeof(MainActivity));
            };

            var storyboard1 = FindViewById(Resource.Id.st1);
            storyboard1.Click += delegate
            {
                var storyboardZoomedActivity = new Intent(this, typeof(StoryboardZoomedActivity));
                storyboardZoomedActivity.PutExtra("StoryboardId", "1");
                StartActivity(storyboardZoomedActivity);
            };

            var storyboard2 = FindViewById(Resource.Id.st2);
            storyboard2.Click += delegate
            {
                var storyboardZoomedActivity = new Intent(this, typeof(StoryboardZoomedActivity));
                storyboardZoomedActivity.PutExtra("StoryboardId", "2");
                StartActivity(storyboardZoomedActivity);
            };

            var mBtnCreateNew = FindViewById(Resource.Id.btnCreateNew);

            mBtnCreateNew.Click += delegate
            {
                //Pull up dialog
                FragmentTransaction transaction = FragmentManager.BeginTransaction();
                DialogStoryboardMenu createStroyboardDialog = new DialogStoryboardMenu();
                createStroyboardDialog.Show(transaction, "dialog fragment");
            };
        }

        private async Task GetStoryboardsOfUser()
        {
            using (var client = new HttpClient())
            {
                var url = "http://storyboardapi20170314091148.azurewebsites.net/Api/Storyboards?mentor_id=" + mentorLoggedInId;
                var result = await client.GetStringAsync(url);

                //mTextView = (TextView)FindViewById(Resource.Id.textViewStickynote);
                //mString = result;
                //mTextView.Text = mString;

            }
        }
    }
}